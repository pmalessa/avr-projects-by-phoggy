/*
 * Simple_LED.c
 *
 * Created: 27.03.2012 19:21:44
 *  Author: Phil
 */

#include<avr/io.h>

#define F_CPU 20000000UL
#include<util/delay.h>

void long_delay(uint16_t ms);

int main(void)
{
	//_delay_ms(1000); //Programmstartverzögerung
	
	DDRD |= (1 << PIND6);// | (1 << PD2);
	uint16_t z= 1000;
	//PORTD = (1 << PD3);
	
	while(1)
	{
		PORTD |= (1 << PIND6);
		long_delay(z);
		PORTD &= ~(1 << PIND6);
		//PORTD = (1 << PD1);
		long_delay(z);
		/*PORTD = (0 << PD1);
		PORTD = (1 << PD2);
		long_delay(z);
		PORTD = (0 << PD2);*/
		}
	return 0;
}

void long_delay(uint16_t ms) {
    for (; ms>0; ms--) _delay_ms(1);
}