/*
 * bike_pov_1284p.c
 * 1284p version
 * last mod. 07.06.2013
 * Author: Phil
 * Copyright RGBike POV Instructable!
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "tlc5940.h"
#include "bike_pov_1284p.h"

#include "pics_8_color/test1.h"


int main(void) {
	//to use in the colour control
	uint8_t colour=0, number_cycles=0, state=0;
/**************************************************************
*	Timer 0 configuration
* It will be used to call the control loop
**************************************************************/
	//CTC mode
	TCCR0A |= (1<<WGM01)|(0<<WGM00);
	//prescaler by 8
	TCCR0B |= (0<<CS02)|(1<<CS01)|(0<<CS00);
	//interrupt with 10kHz frequency
	OCR0A = 30;
	//enable the compare interrupt
	TIMSK0 |= (1<<OCIE0A);
	//enable the timer0
	PRR &= ~(1<<PRTIM0);

/**************************************************************
*	Some more initializations
**************************************************************/
	//Init the TLC5940
	CBI(SENSOR_DDR,SENSOR);
	TLC_Init();
	TLC_Send_GS_Value(0xFF);
	CBI(COLOUR_PORT, COLOUR_BLUE);
	SBI(COLOUR_PORT, COLOUR_GREEN);
	CBI(COLOUR_PORT, COLOUR_RED);
	TLC_Activate();
	TCNT1 = 0x00; //reset GSclk counter
	DDRC |= (0 << DDC5);
	sei(); //global interrupts enable
	_delay_ms(500);

/**************************************************************
*	main cycle
**************************************************************/
	while(1) {
		if(ready_to_send){ //this is done during a GScycle, starting right after Blank
			//turn off all colours
			COLOUR_PORT &= ~((1<<COLOUR_BLUE)|(1<<COLOUR_RED)|(1<<COLOUR_GREEN));
			switch(colour){
				case 0:	//lighting up the BLUES, while sending the REDS
					SBI(COLOUR_PORT, COLOUR_BLUE);
					CBI(BLANK_PORT, BLANK);
					TLC_Send_GS_CTDO(red[matrix_step]);
					break;
				case 1: //lighting up the REDS, while sending the GREENS
					SBI(COLOUR_PORT, COLOUR_RED);
					CBI(BLANK_PORT, BLANK);
					TLC_Send_GS_CTDO(green[matrix_step]);
					break;
				case 2: //lighting up the GREENS, while sending the BLUES
					SBI(COLOUR_PORT, COLOUR_GREEN);
					CBI(BLANK_PORT, BLANK);
					TLC_Send_GS_CTDO(blue[matrix_step]);
					break;
			}
			ready_to_send = 0; //clearing GSclock flag

			colour++;	//next colour
			if(colour == 3) { //rinse and repeat
				colour = 0;
				number_cycles++;
				if(number_cycles >= cycles) { //finished the number of repetitions
					number_cycles = 0;
					matrix_step++;			//if so, next step
					if(matrix_step >= wheel_divisions)  {
						spins++;	//for feedback loop
						matrix_step = 0;	
					}
				}
			}
		}
	}
	return 0;
}


/**************************************************************
*	Control loop call 10kHz frequency
**************************************************************/
ISR (TIMER0_COMPA_vect){
	//detect rising edge when the magnet is detected
	sensor = (SENSOR_PIN & (1<<SENSOR));
	if(sensor && !sensor_prev) {
		if(spins>0) { //if the system has already pass through the entire matrix, it is displaying too fast!
			cycles +=  matrix_step/6;
			spins=0;
		} else { //if the system hasn't yet pass through the entire matrix, it is displaying too slow!
			cycles -= (wheel_divisions - matrix_step)/6;
		}
		if(cycles<0) cycles = 1;
		matrix_step=0;
	}
	sensor_prev = sensor;
}