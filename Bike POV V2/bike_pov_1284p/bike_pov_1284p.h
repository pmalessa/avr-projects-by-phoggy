#ifndef _BIKE_POV_H_

#define SBI(x,y) (x |= (1<<y)); 				/* set bit y in byte x */ 
#define CBI(x,y) (x &= (~(1<<y))); 				/* clear bit y in byte x */ 

#define SENSOR_PORT PORTB
#define SENSOR_PIN PINB
#define SENSOR_DDR DDRB
#define SENSOR PINB1

//const int8_t wheel_divisions=80;	//number of lines that the image is composed
volatile int8_t cycles=70;			//number of repetition of each line, will be controlled by sensor feedback
volatile uint8_t matrix_step=0;		//current matrix step
volatile uint8_t sensor_prev=1, sensor=1;//for detecting the edge
volatile uint8_t spins=0;			//for the feedback loop

#endif //_BIKE_POV_H_
