#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "tlc5940.h"

volatile uint8_t ready_to_send = 0;


/**************************************************
* Reset the GScounter interrupt
***************************************************/
ISR (TIMER1_COMPA_vect){
	SBI(BLANK_PORT, BLANK);
	SBI(XLAT_PORT, XLAT);
	ready_to_send = 1;
	CBI(XLAT_PORT, XLAT);
}

/**************************************************
* Activates the TLC
***************************************************/
inline void TLC_Activate() {
	CBI(BLANK_PORT,BLANK);
}

/**************************************************
* Deactivates the TLC
***************************************************/
inline void TLC_Deactivate() {
	SBI(BLANK_PORT,BLANK);
}

/**************************************************
* TLC in DC mode
***************************************************/
inline void TLC_DC() {
	SBI(VPROG_PORT,VPROG);
}

/**************************************************
* TLC in GS mode
***************************************************/
inline void TLC_GS() {
	CBI(VPROG_PORT,VPROG);
}


/**************************************************
* Send GS data
* ---------------------
* Should receive a 24 byte array adress in program
* memory, to send 192 bits
***************************************************/
void TLC_Send_GS(const uint8_t *data) {
	uint8_t i;
	for(i=0; i<24; i++){
		SPDR = pgm_read_byte(data);	//start sending
		while (!(SPSR & (1<<SPIF))); //wait until not sent	
		data++;
	}
}

/**************************************************
* Send GS CTDO
* ---------------------
* Should receive a 16 byte array adress in program
* memory, to send 192 bits
***************************************************/
void TLC_Send_GS_CTDO(const uint8_t *data) {
	uint8_t i;
	uint32_t val;
	uint16_t byte1, byte2;

	for(i=0; i<8; i++) {
		byte1 = pgm_read_byte(data++) << 4;
		byte2 = pgm_read_byte(data++) << 4;

		SPDR = (byte1 >> 4) & 0xff;
		while (!(SPSR & (1<<SPIF)));
		
		SPDR = ( (byte1 << 4) & 0xf0 ) | ((byte2 >> 8) & 0x0f );
		while (!(SPSR & (1<<SPIF)));
		
		SPDR = byte2 & 0xff;
		while (!(SPSR & (1<<SPIF)));
	}
}

/**************************************************
* Send GS data FLASH
* ---------------------
* Should receive a 24 byte array adress in FLASH
* memory, to send 192 bits
***************************************************/
void TLC_Send_GS_FLASH(uint8_t *data) {
	uint8_t i;
	for(i=0; i<24; i++){
		SPDR = data[i];	//start sending
		while (!(SPSR & (1<<SPIF))); //wait until not sent
	}
}

/**************************************************
* Send GS data 2
* ---------------------
* Should receive two 12 byte array adresses in program
* memory, to send 192 bits
***************************************************/
void TLC_Send_GS2(uint8_t *data, uint8_t *data2) {
	uint8_t i;
	for(i=0; i<12; i++){
		SPDR = data[i];	//start sending
		while (!(SPSR & (1<<SPIF))); //wait until not sent	
		//data++;
	}		
	for(i=0; i<12; i++){
		SPDR = data2[i];	//start sending
		while (!(SPSR & (1<<SPIF))); //wait until not sent	
		//data2++;
	}
}

/**************************************************
* Send GS data RED TEST
* ---------------------
* Should receive a 16 byte array address in program memory,
* but with data for all colours.
* w = white, r = red, g = green, b = blue
* v = violet, y = yellow, t = turkey, n = black
***************************************************/
void TLC_Send_GS_R_TEST(uint8_t *data) {
	uint8_t i = 0, tmp1 = 0, tmp2 = 0;
	for(; i<8; i++){
		tmp1 = *data;
		data++;
		tmp2 = *data;
		data++;
		if(tmp1 & (1<<BIT_RED)){ //if tmp1 is red
			if (tmp2 & (1<<BIT_RED)){ //and tmp2 is red
				//array[j]= 0xFF;	j++;
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 red, tmp2 not red
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xF0;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
		}}else{
			if (tmp2 & (1<<BIT_RED)){ //tmp1 not red, tmp2 red
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x0F;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 not red, tmp2 not red
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}
		}
	}	
}
/**************************************************
* Send GS data GREEN TEST
* ---------------------
* Should receive a 16 byte array address in program memory,
* but with data for all colours.
* w = white, r = red, g = green, b = blue
* v = violet, y = yellow, t = turkey, n = black
***************************************************/
void TLC_Send_GS_G_TEST(uint8_t *data) {
	uint8_t i = 0, tmp1 = 0, tmp2 = 0;
	for(; i<8; i++){
		tmp1 = *data;
		data++;
		tmp2 = *data;
		data++;
		if(tmp1 & (1<<BIT_GREEN)){ 
			if (tmp2 & (1<<BIT_GREEN)){ //tmp1 green, tmp2 green
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 green, tmp2 not green
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xF0;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
		}}else{
			if (tmp2 & (1<<BIT_GREEN)){ //tmp1 not green, tmp2 green
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x0F;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 not green, tmp2 not green
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}
		}
	}
// 	for(;k<24; k++){
// 			SPDR = array[j];
// 			while (!(SPSR & (1<<SPIF))); //wait until not sent	
// 			j++;
// 	}		
}
/**************************************************
* Send GS data BLUE TEST
* ---------------------
* Should receive a 16 byte array address in program memory,
* but with data for all colours.
* 7 = white, 4 = red, 2 = green, 1 = blue
* 5 = violet, 6 = yellow, 3 = turkey, 0 = black
***************************************************/
void TLC_Send_GS_B_TEST(uint8_t *data) {
	uint8_t i = 0, tmp1 = 0, tmp2 = 0;
	for(; i<8; i++){
		tmp1 = *data;
		data++;
		tmp2 = *data;
		data++;
		if(tmp1 & (1<<BIT_BLUE)){
			if (tmp2 & (1<<BIT_BLUE)){ //tmp1 blue, tmp2 blue
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 blue, tmp2 not blue
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xF0;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
		}}else{
			if (tmp2 & (1<<BIT_BLUE)){ //tmp1 not blue, tmp2 blue
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x0F;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0xFF;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}else{ //tmp1 not blue, tmp2 not blue
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
				SPDR = 0x00;
				while (!(SPSR & (1<<SPIF))); //wait until not sent
			}
		}
	}
// 		for(;k<24; k++){
// 			j = 0;	
// 			SPDR = array[j];
// 			while (!(SPSR & (1<<SPIF))); //wait until not sent	
// 			j++;
// 		}	
}
/**************************************************
* Insert in Array
* ---------------------
* Should receive an integer 0-99 and a color.
* This will insert the number properly into the right array
***************************************************/
/**void Insert_In_Array(uint8_t number, uint8_t color){
	uint8_t zehner = 0;
	uint8_t einser = 0;
	uint8_t i;
	uint8_t j;
	uint8_t *data;
	zehner = number/10;
	einser = number - zehner;
	
	for(j=1; j<=8; j++){
		data = a[j];
		for(i=1; i<=12; i++){
			red[j][i] = pgm_read_byte(data);
			data++;
		}
	}
	
		for(j=1; j<=8; j++){
		data = a[j];
		for(i=1; i<=12; i++){
			red[j+10][i] = pgm_read_byte(data);
			data++;
		}
	}
}**/

/**************************************************
* Send single GS value
* ---------------------
* This sends a single value for all the leds
***************************************************/
void TLC_Send_GS_Value(uint8_t data) {
	uint8_t i;
	for(i=0; i<24; i++){
		SPDR = data;	//start sending
		while (!(SPSR & (1<<SPIF))); //wait until not sent	
	}
}

/**************************************************
* Send single DC value
* ---------------------
* First, set the TLC in DC mode
* This sends a single value for all the leds
***************************************************/
void TLC_Send_DC_Value(uint8_t data) {
	uint8_t i;

	for(i=0; i<12; i++){
		SPDR = data;	//start sending
		while(!(SPIF & SPIF)); //wait until not sent
	}
	SBI(XLAT_PORT,XLAT); //bump the XLAT
	CBI(XLAT_PORT,XLAT);
}

/**************************************************
* Initializes etc etc
***************************************************/
void TLC_Init(){
//setting the port pins
	// set all the tlc pins as outputs
	SBI(XLAT_DDR,XLAT);
	SBI(BLANK_DDR,BLANK);
	SBI(VPROG_DDR, VPROG);
	SBI(DCPROG_DDR, DCPROG);
	//
	CBI(XLAT_PORT,XLAT);  
	SBI(BLANK_PORT,BLANK); // deactivate the tlc, blank as high
	CBI(VPROG_PORT, VPROG); //GS mode
	CBI(DCPROG_PORT, DCPROG); //dot correction from EEPROM (0x3F by default)
	//SBI(DCPROG_PORT, DCPROG); //dot correction from register
	
	// set the colour selector pins as output, and deactivate
	SBI(COLOUR_DDR, COLOUR_RED);
	SBI(COLOUR_DDR, COLOUR_GREEN);
	SBI(COLOUR_DDR, COLOUR_BLUE);
	CBI(COLOUR_PORT, COLOUR_RED);
	CBI(COLOUR_PORT, COLOUR_GREEN);
	CBI(COLOUR_PORT, COLOUR_BLUE);


//Set timer for GSclk
	//CTC with TOP value on OCR1A
	TCCR1A |= (0<<WGM11) | (0<<WGM10);
	TCCR1B |= (0<<WGM13) | (1<<WGM12);

	//set value for compare, 1 tic before 0xFFF/4094
	OCR1AH = 0x0F;
	OCR1AL = 0xFB;

	//enable the timer1 on PowerReductions Register
	PRR &= ~(1<<PRTIM1);

	//set clock source, no prescalling from cklio
	TCCR1B |= (0<<CS12)|(0<<CS11)|(1<<CS10);

	//set interrupt when compare with regA
	TIMSK1 |= (1<<OCIE1A);

//activate SPI in master mode to output data
	//set the MOSI,SCK and /SS pins as outputs
	SBI(SIN_DDR, SIN);
	SBI(SIN_DDR, SCL);
	SBI(SIN_DDR, PINB2); //!SS pin

	//enable the SPI on Power Reductions Register
	PRR &= ~(1<<PRSPI);

	//set clk 2x faster
	SPSR = (1<<SPI2X);

	//enable, set MSB first, set as master, set lowest clk divide
	SPCR = (1<<SPE)|(0<<DORD)|(1<<MSTR)|(0<<CPOL)|(0<<CPHA) |(0<<SPR1)|(0<<SPR0);
}
