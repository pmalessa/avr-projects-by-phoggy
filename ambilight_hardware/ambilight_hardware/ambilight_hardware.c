/*
 * ambilight_hardware.c
 *
 * Created: 07.04.2013 18:50:09
 *  Author: Phil
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include "irmp.h"
#include "ambilight_hardware.h"

#ifndef F_CPU
#error F_CPU unkown
#endif

int command = 0;
int counter = 0;
int speed = 15000/255;


void
timer1_init (void)
{
	#if defined (__AVR_ATtiny45__) || defined (__AVR_ATtiny85__)                // ATtiny45 / ATtiny85:
	#if F_CPU >= 16000000L
	OCR1C   =  (F_CPU / F_INTERRUPTS / 8) - 1;                              // compare value: 1/15000 of CPU frequency, presc = 8
	TCCR1   = (1 << CTC1) | (1 << CS12);                                    // switch CTC Mode on, set prescaler to 8
	#else
	OCR1C   =  (F_CPU / F_INTERRUPTS / 4) - 1;                              // compare value: 1/15000 of CPU frequency, presc = 4
	TCCR1   = (1 << CTC1) | (1 << CS11) | (1 << CS10);                      // switch CTC Mode on, set prescaler to 4
	#endif
	#else                                                                       // ATmegaXX:
	OCR1A   =  (F_CPU / F_INTERRUPTS) - 1;                                  // compare value: 1/15000 of CPU frequency
	TCCR1B  = (1 << WGM12) | (1 << CS10);                                   // switch CTC Mode on, set prescaler to 1
	#endif
	#ifdef TIMSK1
	TIMSK1  = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
	#else
	TIMSK   = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
#endif
}
#ifdef TIM1_COMPA_vect                                                      // ATtiny84
#define COMPA_VECT  TIM1_COMPA_vect
#else
#define COMPA_VECT  TIMER1_COMPA_vect                                   // ATmega
#endif

int main(void)
{
	// this code sets up counter0 for an 8kHz Fast PWM wave @ 16Mhz Clock
    DDRD |= (1 << DDD5)|(1 << DDD6);	   // PD5 and PD6 are now outputs
	COLOR_RED = 255;							   // set PWM for 25% duty cycle
	COLOR_GREEN = 255;							   // set PWM for 50% duty cycle
    TCCR0A |= (1 << COM0A1)|(1 << COM0B1); // set none-inverting mode
    TCCR0A |= (1 << WGM01) | (1 << WGM00); // set fast PWM Mode
    TCCR0B |= (1 << CS01);				   // START the timer with 8x prescaler
	
	// this code sets up counter2 A output at 25% and B output at 75% 
	// 8 bit Fast PWM.			   // PB3...
	DDRD |= (1 << DDD3);				   // ...and PD3 are now outputs
    COLOR_BLUE = 255;						   // set PWM for 75% duty cycle @ 8bit
    TCCR2A |= (1 << COM2B1); // set none-inverting mode
    TCCR2A |= (1 << WGM21) | (1 << WGM20);
    TCCR2B |= (1 << CS21);				   // START the timer with 8x prescaler
	
	IRMP_DATA irmp_data;

	irmp_init();                                                            // initialize irmp
	timer1_init();                                                          // initialize timer1
	long_delay(500);
	COLOR_BLUE = 0;
	COLOR_GREEN = 0;
	COLOR_RED = 0;
	sei();
	
	while (1)
    {
		if (irmp_get_data (&irmp_data))
		{
			// ir signal decoded, do something here...
			// irmp_data.protocol is the protocol, see irmp.h
			// irmp_data.address is the address/manufacturer code of ir sender
			// irmp_data.command is the command code
			// irmp_protocol_names[irmp_data.protocol] is the protocol name (if enabled, see irmpconfig.h)
			command = irmp_data.command;
			//if(irmp_data.command == COM_QUICK)speed = speed - speed / 2;
			//if(irmp_data.command == COM_SLOW)speed = speed + speed / 2;
		}
    }
}

ISR(TIMER1_COMPA_vect)                                                             // Timer1 output compare A interrupt service routine, called every 1/15000 sec
{
	(void) irmp_ISR();                                                        // call irmp ISR
	// call other timer interrupt routines...
	// called 15.000 times per sec
	if(command == COM_RED)		func_red();
	if(command == COM_GREEN)	func_green();
	if(command == COM_BLUE)		func_blue();
	if(command == COM_WHITE)	func_white();
}

void long_delay(uint16_t ms) {
	for (; ms>0; ms--) _delay_ms(1);
}

void func_red(){
	command = 0;
	COLOR_RED = 255;
	COLOR_GREEN = 0;
	COLOR_BLUE = 0;
}
void func_green(){
	command = 0;
	COLOR_RED = 0;
	COLOR_GREEN = 255;
	COLOR_BLUE = 0;
}
void func_blue(){
	command = 0;
	COLOR_RED = 0;
	COLOR_GREEN = 0;
	COLOR_BLUE = 255;
}
void func_white(){
	command = 0;
	COLOR_RED = 255;
	COLOR_GREEN = 255;
	COLOR_BLUE = 255;
}