/*
 * ambilight_hardware.h
 *
 * Created: 13.04.2013 18:03:32
 *  Author: Phil
 */ 


#ifndef AMBILIGHT_HARDWARE_H_
#define AMBILIGHT_HARDWARE_H_

	#define COLOR_RED				OCR0A
	#define COLOR_GREEN				OCR0B
	#define COLOR_BLUE				OCR2B
//REMOTE KEYS
	//Line 1
	#define COM_BRIGHT_UP			0x005C
	#define COM_BRIGHT_DOWN			0x005D
	#define COM_PLAY_PAUSE			0x0041
	#define COM_POWER				0x0040
	//Line 2
	#define COM_RED					0x0058
	#define COM_GREEN				0x0059
	#define COM_BLUE				0x0045
	#define COM_WHITE				0x0044
	//Line 3
	#define COM_RED_ORANGE			0x0054
	#define COM_GREEN_CYAN			0x0055
	#define COM_BLUE_VIOLET			0x0049
	#define COM_BRIGHT_PINK			0x0048
	//Line 4
	#define COM_ORANGE				0x0050
	#define COM_CYAN				0x0051
	#define COM_VIOLET				0x004D
	#define COM_PINK				0x004C
	//Line 5
	#define COM_ORANGE_YELLOW		0x001C
	#define COM_CYAN_BLUE			0x001D
	#define COM_VIOLET_PINK			0x001E
	#define COM_BRIGHT_CYAN			0x001F
	//Line 6
	#define COM_YELLOW				0x0018
	#define COM_BRIGHT_BLUE			0x0019
	#define COM_DARK_PINK			0x001A
	#define COM_WHITE_CYAN			0x001B
	//Line 7
	#define COM_RED_UP				0x0014
	#define COM_GREEN_UP			0x0015
	#define COM_BLUE_UP				0x0016
	#define COM_QUICK				0x0017
	//Line 8
	#define COM_RED_DOWN			0x0010
	#define COM_GREEN_DOWN			0x0011
	#define COM_BLUE_DOWN			0x0012
	#define COM_SLOW				0x0013
	//Line 9
	#define COM_DIY_1				0x000C
	#define COM_DIY_2				0x000D
	#define COM_DIY_3				0x000E
	#define COM_AUTO				0x000F
	//Line 10
	#define COM_DIY_4				0x0008
	#define COM_DIY_5				0x0009
	#define COM_DIY_6				0x000A
	#define COM_FLASH				0x000B
	//Line 11
	#define COM_JUMP_3				0x0004
	#define COM_JUMP_7				0x0005
	#define COM_FADE_3				0x0006
	#define COM_FADE_7				0x0007

void long_delay(uint16_t);
void func_red(void);
void func_green(void);
void func_blue(void);
void func_white(void);
#endif /* AMBILIGHT_HARDWARE_H_ */