/*
 * bikepov_bootloader.c
 *
 * Created: 17.04.2013 11:58:41
 *  Author: Phil
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/boot.h>
#include <util/delay.h>
#include <eeprom.h>
#include "uart.h"
#include "24c64.h"
 
#define F_CPU 20000000UL
#define BOOT_UART_BAUD_RATE     9600     /* baudrate */
#define XON                     17       /* XON sign */
#define XOFF                    19       /* XOFF sign */
#define PIC_START				0x3F	 /* ? - command for EEPROM save routine*/
#define PIC_STOP				0x21	 /* ! - command for EEPROM save routine*/
#define INT_EEPROM_SIZE				2048	 /* size of internal EEPROM in Bytes*/
#define EXT_EEPROM_SIZE				32768	 /* size of external EEPROM in Bytes*/
 
void pic_2_eeprom(void);
 
int main()
{
	unsigned int	adress = 0;
    unsigned int 	c=0;               /* Empfangenes Zeichen + Statuscode */
    unsigned char	temp,              /* Variable */
                    flag=1,            /* Flag zum steuern der Endlosschleife */
					p_mode=0;		   /* Flag zum steuern des Programmiermodus */
    void (*start)( void ) = 0x0000;    /* Funktionspointer auf 0x0000 */
 
    /* Interrupt Vektoren verbiegen */
 
    char sregtemp = SREG;
    cli();
    temp = MCUCR;
    MCUCR = temp | (1<<IVCE);
    MCUCR = temp | (1<<IVSEL);
    SREG = sregtemp;
 
    /* Einstellen der Baudrate und aktivieren der Interrupts */
    uart_init( UART_BAUD_SELECT(BOOT_UART_BAUD_RATE,F_CPU) );
	EEOpen(); 
    sei();
 
    uart_puts("Hallo hier ist der Bootloader\n\r");
	uart_puts("e - write pic to eeprom, q - quit bootloader\n\r");
 
    do
    {
        c = uart_getc();
        if( !(c & UART_NO_DATA) )
        {
            switch((unsigned char)c)
            {
                 case 'q': 
					 flag=0;
                     uart_puts("Verlasse den Bootloader!\n\r");
                     break;
				 case 'e':
					 pic_2_eeprom();
				 
                  default:
                     uart_puts("Du hast folgendes Zeichen gesendet: ");
                     uart_putc((unsigned char)c);
                     uart_puts("\n\r");
                     break;
            }
        }
    }
    while(flag);
 
    uart_puts("Springe zur Adresse 0x0000!\n\r");
    _delay_ms(1000);
 
    /* vor Rücksprung eventuell benutzte Hardware deaktivieren
       und Interrupts global deaktivieren, da kein "echter" Reset erfolgt */
 
    /* Interrupt Vektoren wieder gerade biegen */
    cli();
    temp = MCUCR;
    MCUCR = temp | (1<<IVCE);
    MCUCR = temp & ~(1<<IVSEL);
 
    /* Rücksprung zur Adresse 0x0000 */
    start(); 
    return 0;
}

void pic_2_eeprom(){
	//check for existing pics in eeprom.
	//external eeprom saves the pic
	//internal eeprom saves the num_pics
	int cur_adress = 0;		//current adress, counter for scan process
	int num_pics = 0;		//number of exiting pictures in eeprom, increased at scan process
	int buffer = 0;			//buffer for scan process
	int scan_done = 0;		//bool for loop
	int pic_start_com = 0;	//pic start command detected
	uint8_t startH = 0;		//Start adress high bit
	uint8_t startM = 0;		//Start adress mid bit
	uint8_t startL = 0;		//Start adress low bit
	uint8_t cur_pic = 0;	//current picture
	uint8_t error = 0;		//error variable, 1 if theres an error
	
	while(!scan_done){
		buffer = eeprom_read_byte(cur_adress);
		if (buffer == PIC_START){								//if there is a pic start command in buffer
			cur_adress++;										//move to the next byte
			num_pics = eeprom_read_byte(cur_adress);			//...and read the number of pics.
			if(eeprom_read_byte(cur_adress++) == PIC_STOP){
				scan_done = 1;
				break;
			}
		}
		cur_adress++;
	}
	
	/* syntax in the internal eeprom:
	/*| 1 |    2     | 3 |*/
	/*--------------------*/
	/*| ? | num_pics | ! |*/
	
	/* syntax in the external eeprom:
	/*| 1 |    2     | 3 |...*/
	/*---------------------------------------------------------------------------------*/
	/*| ? | pic_nr	 | @ | pic data first row | # | pic data second row | # | ... | ! |*/
	
	uart_puts("There are ");
	uart_putc(num_pics);
	uart_puts(" pics in the external EEPROM.\n\r");
	uart_puts("1 - ");
	uart_putc(num_pics);
	uart_putc(" = overwriting existing pic.\n")
	uart_putc(num_pics+1);
	uart_puts(" = saving after the last pic.")
	scan_done = 0;
	while(scan_done){
		switch(uart_getc()){
			case '1':{cur_pic = 1; scan_done = 1; break;}
			case '2':{cur_pic = 2; scan_done = 1; break;}
			case '3':{cur_pic = 3; scan_done = 1; break;}
			case '4':{cur_pic = 4; scan_done = 1; break;}
			case '5':{cur_pic = 5; scan_done = 1; break;}
			case '6':{cur_pic = 6; scan_done = 1; break;}
			case '7':{cur_pic = 7; scan_done = 1; break;}
			case '8':{cur_pic = 8; scan_done = 1; break;}
			case '9':{cur_pic = 9; scan_done = 1; break;}
		}
		if(scan_done){
			if(!(cur_pic (<= num_pics+1))){
				scan_done = 0;
				uart_puts("wrong number");
			}
		}
	}
	
	
	
	
	
	
}