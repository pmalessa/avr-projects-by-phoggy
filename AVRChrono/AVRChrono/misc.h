/*
 * misc.h
 *
 * Created: 20.05.2014 19:33:41
 *  Author: Phil
 */ 


#ifndef MISC_H_
#define MISC_H_

/************************************************************************/
/* Misc Defines                                                         */
/************************************************************************/
#define TRUE 1
#define FALSE 0

/************************************************************************/
/* Function Prototypes                                                  */
/************************************************************************/

void calc_cur();
void pcint_init();
int timer16_init();
int service_menu();
void check_buttons();


#endif /* MISC_H_ */