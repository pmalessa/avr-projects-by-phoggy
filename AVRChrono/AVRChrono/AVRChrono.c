/*
 * AVRChrono.c
 *
 * Created: 14.05.2014 08:14:32
 *  Author: Phil
 * 
 * AVRChrono - a Chronometer build out of an atmega328p, a lcd display (16x2) and
 * two light gates.
 * run with 20MHz clock
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include "lcd-routines.h"
#include "misc.h"

volatile uint16_t	  s_time = 0,
					  k_time = 0, 
					  m_time = 0, 
					  b_time = 0,
					  time_step = 0,
					  s_time_capt = 0,
					  k_time_capt = 0,
					  m_time_capt = 0,
					  b_time_capt = 0;
char display_l1[17],
	 display_l2[17];
unsigned int bmenu_pressed = 0,
			 bplus_pressed = 0,
			 bminus_pressed = 0, 
			 breset_pressed = 0,
			 menu_index = 10,
			 button_timeout = 0,
			 calc_ready = FALSE,
			 f_sensor_triggered = FALSE;
double cur_mps = 0.0,
	   cur_joule = 0.0,
	   cur_fps = 0.0,
	   avg_mps = 32.768,
	   avg_joule = 0.0,
	   avg_fps = 0.0,
	   weight = 0.20,
	   distance_mm = 98.0,
	   tot_time = 0.0;

int timer16_init()
{
	//config Timer 1 - 16bit
	TCCR1B = (1<<CS11); //Prescaler by 8
	TCCR1B |= (1<<WGM12); //CTC Mode
	//20.000.000 Hz /8 (Prescaler /10.000 = 250	//every 0.1 ms
	OCR1A = 250-1;
	
	TIMSK1 |= (1<<OCIE1A);	//turn on compare interrupt
	return 0;
}

void pcint_init()
{
	PCICR |= (1 << PCIE0)|(1 << PCIE2);	//PCINT0 and PCINT2 registers
	PCMSK0 |= (1 << PCINT0);			//PCINT0 interrupt
	PCMSK2 |= (1 << PCINT23);			//PCINT23 interrupt
}

int service_menu()
{
	if(calc_ready)
	{
		calc_ready = FALSE;
		calc_cur();
	}
	lcd_home();
	switch(menu_index)
	{
		case 0:
			sprintf(display_l1,"voll heftiges   ");
			sprintf(display_l2,"AVR-Chrono!     ");
			break;
		case 10:
			sprintf(display_l1," current [%1.2fg]",weight);
			sprintf(display_l2,"%3.0f m/s - %1.2f J",cur_mps,cur_joule);
			break;
		case 20:
			sprintf(display_l1," average [%1.2fg]",weight);
			sprintf(display_l2,"%3.0f m/s - %1.2f J",avg_mps,avg_joule);
			break;
		case 30:
			sprintf(display_l1," current [%1.2fg]",weight);
			sprintf(display_l2,"%3.0f fps - %1.2f J",cur_fps,cur_joule);
			break;
		case 40:
			sprintf(display_l1," average [%1.2fg]",weight);
			sprintf(display_l2,"%3.0f fps - %1.2f J",avg_fps,avg_joule);
			break;
				
		default:
			menu_index = 0;
			break;
	}
	check_buttons();
	if(bmenu_pressed)
	{
		bmenu_pressed = FALSE;
		menu_index += 10;
	}
	if(bplus_pressed)
	{
		bplus_pressed = FALSE;
		weight+=0.01;
	}
	if(bminus_pressed)
	{
		bminus_pressed = FALSE;
		weight-=0.01;
	}
	if(breset_pressed)
	{
		breset_pressed = FALSE;
		cur_mps = 0;
		cur_joule = 0;
		avg_mps = 0;
		avg_joule = 0;
		}
	lcd_string(display_l1);
	lcd_setcursor(0,2);
	lcd_string(display_l2);
	return 0;
}

void check_buttons()
{
	if(button_timeout > 4000)
	{
		if(!(PIND & (1<<PIND1))){ bmenu_pressed=TRUE; button_timeout = 0;}
		if(!(PIND & (1<<PIND2))){ bminus_pressed=TRUE; button_timeout = 0;}
		if(!(PIND & (1<<PIND3))){ bplus_pressed=TRUE; button_timeout = 0;}
		if(!(PIND & (1<<PIND4))){ breset_pressed=TRUE; button_timeout = 0;}
	}
}

void calc_cur()
{
	tot_time = 0.0;
	if(m_time_capt) tot_time += m_time_capt * 100000.0;
	if(k_time_capt) tot_time += k_time_capt * 100.0;
	if(s_time_capt) tot_time += (s_time/10.0);
	tot_time += ((time_step/10.0)/250.0);
	cur_mps = distance_mm/tot_time;
	
	cur_fps = cur_mps * 3.28084;
	cur_joule =weight * cur_mps * cur_mps / 2000.0;
	PORTD |= (1<<PIND6);
}

int main(void)
{
	DDRD &= ~( ( 1 << PIND1 ) | ( 1 << PIND2 ) | ( 1 << PIND3 ) | ( 1 << PIND4 ) | ( 1 << PIND7 ));		// PIN PD1 - PD4 : Button Input, PIN D7 : front Sensor Input
	DDRB &= ~( (1 << PINB0));																			//PIN PB0 : back Sensor Input
	PORTD |=    (1 << PIND1 ) | ( 1 << PIND2 ) | ( 1 << PIND3 ) | ( 1 << PIND4 );						//Enable Pull Up Resistor on Button Input
	DDRD |= (1 << PIND6);																				//PIN PD6 : Debug-LED Output
	
	pcint_init();		//Init of Sensor Interrupts
	timer16_init();		//Init counting Timer
	lcd_init();			//Init LCD-Display
	sei();				//Enable global Interrupts
    while(1)
    {
		service_menu();	//Call Display menu
    }
	return 0;
}

ISR (TIMER1_COMPA_vect)
{
	s_time++;		//0.1 ms steps
	button_timeout++;
	if(s_time >= 1000)
	{
		s_time = 0;
		k_time++;	//100ms steps
		if(k_time >= 1000)
		{
			k_time = 0;
			m_time++;	//100s steps
			if(m_time >= 1000)
			{
				m_time = 0;
			}
		}
	}
}

ISR (PCINT2_vect)
{
	//Interrupt front sensor
	cli();					//Disable global Interrupts
	f_sensor_triggered = TRUE;
	PORTD &= ~(1 << PIND6);	//Turn off Debug-LED
	TIMSK1 &= ~(1<<OCIE1A); //stop Timer
	TCNT1 = 0;				//reset Timer
	s_time = 0; k_time = 0;	//reset counting variables
	m_time = 0; b_time = 0;
	TIMSK1 |= (1<<OCIE1A);	//restart  Timer
	sei();					//enable global Interrupts
}

ISR (PCINT0_vect)
{
	//Interrupt back sensor
	cli();					//Disable global Interrupts
	TIMSK1 &= ~(1<<OCIE1A); //stop Timer
	time_step = TCNT1;		//read out Timer
	s_time_capt = s_time;	//read out counter
	k_time_capt = k_time;
	m_time_capt = m_time;
	b_time_capt = b_time;
	if(f_sensor_triggered == TRUE)
	{
		calc_ready = TRUE;	//only calculate, if both sensors passed. Supresses multiple Interrupts
		f_sensor_triggered = FALSE;
	}
	TIMSK1 |= (1<<OCIE1A);	//restart Timer
	sei();					//Enable global Interrupts
}
