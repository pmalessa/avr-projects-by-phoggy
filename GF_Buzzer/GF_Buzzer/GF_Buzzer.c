/*
 * GF_Buzzer.c
 *
 * Created: 14.06.2013 07:56:12
 *  Author: Phil
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
//#include <util/delay.h>
#include <avr/sfr_defs.h>

void flashLED();
void lightLED(uint8_t led);
uint8_t Session = 1;

int main(void)
{
	//initialize pins
	//DDR Ports: Input for Buzzer and Buttons, Output for LEDs
	DDRC |= (1 << PC5) | (1 << PC4) | (1 << PC3) | (1 << PC2);
	DDRB &= ~(1 << PB7);
	DDRD &= ~(1 << PD5);
	DDRD &= ~(1 << PD6);
	DDRD &= ~(1 << PD7);
	DDRD &= ~(1 << PD2);
	DDRB |= (1 << PB2);
	//activate internal PullUps
	PORTB |= (1 << PB7);
	PORTD |= (1 << PD5) | (1 << PD6) | (1 << PD7);
	//Buzzer Interrupt Setup
	//Pullup on Buzzer Input
	PORTD |= (1 << PD2);
	//Interrupt if INT0 is RISING
	MCUCR |= (1<<ISC01)|(0<<ISC00);
	//activate Interrupt on INT0
	GICR |= (1<<INT0);
	//turn on interrupts
	//sei();
    while(1)
    {
		if(bit_is_clear(PIND, PD2))
		{
			if(bit_is_clear(PINB, PB7)){lightLED(1); Session=0;}
			else if(bit_is_clear(PIND, PD5)){lightLED(2); Session=0;}
			else if(bit_is_clear(PIND, PD6)){lightLED(3); Session=0;}
			else if(bit_is_clear(PIND, PD7)){lightLED(4); Session=0;}
		}
    }
}

void lightLED(uint8_t led)
{
	//toggle buzzer button
	PORTB |= (1<< PB2);
	//_delay_ms(100);
	PORTB &= ~(1<< PB2);
	//wait
	//_delay_ms(2000);
	//show winner
	switch (led)
	{
	case 1:
	{
		for(int i=0;i<10;i++)
		{
			PORTC |= (1<< PC5);
			//_delay_ms(200);
			PORTC &= ~(1<< PC5);
			//_delay_ms(200);
		}
		break;
	}
	case 2:
	{
		for(int i=0;i<10;i++)
		{
			PORTC |= (1<< PC4);
			//_delay_ms(200);
			PORTC &= ~(1<< PC4);
			//_delay_ms(200);
		}
		break;
	}
	case 3:
		{
			for(int i=0;i<10;i++)
			{
				PORTC |= (1<< PC3);
				//_delay_ms(200);
				PORTC &= ~(1<< PC3);
				//_delay_ms(200);
			}
		break;	
		}
	case 4:
		{ 
			for(int i=0;i<10;i++)
			{
				PORTC |= (1<< PC2);
				//_delay_ms(200);
				PORTC &= ~(1<< PC2);
				//_delay_ms(200);
			}
		break;
		}
	//turn on interrupts
	//sei();
	}
}

void flashLED()
{
	//turn on all leds
	PORTC = 0xFF;
	//_delay_ms(100);
	//turn off all LEDs
	PORTC &= ~0xFF;
}

ISR(INT0_vect)
{
	//turn off interrupts
	cli();
	Session=1;
	flashLED();
}