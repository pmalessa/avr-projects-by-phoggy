/*
 * LED_Steuerung_Seriell.c
 *
 * Created: 31.05.2012 17:31:59
 *  Author: Phil
 */ 
#define R 0x52
#define G 0x47
#define B 0x42

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "uart.h"


/* define CPU frequency in MHz here if not defined in Makefile */
#define F_CPU 14745600UL
#include <util/delay.h>

/* 9600 baud */
#define UART_BAUD_RATE      9600    


int main(void)
{
	DDRD  |= (1 << PD5) | (1 << PD6) | (1 << PD7);
	PORTD |= (1 << PD5) | (1 << PD6) | (1 << PD7);
	unsigned int c;
	
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU));
	sei();
	
	uart_puts("Welcome to LED Control.....Type R,G or B to turn color on or off...");
    while(1)
    {
       c = uart_getc();
	   if (c & R)
	   {
		   //Turn red on or off
		   if (PORTD == (1 << PD5))
		   {
			   PORTD = (0 << PD5);
		   } 
		   else
		   {
			   PORTD = (1 << PD5);
		   }
	   }
	   if (c & G)
	   {
		   //Turn red on or off
		   if (PORTD == (1 << PD6))
		   {
			   PORTD = (0 << PD6);
		   } 
		   else
		   {
			   PORTD = (1 << PD6);
		   }
	   }
	   if (c & B)
	   {
		   //Turn red on or off
		   if (PORTD == (1 << PD7))
		   {
			   PORTD = (0 << PD7);
		   } 
		   else
		   {
			   PORTD = (1 << PD7);
		   }
	   }
    }
}