/*
 * UART_Testprogramm.c
 *
 * Created: 27.03.2012 18:34:29
 *  Author: Phil
 *	atmega328p
 */ 

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#include "uart.h"


/* define CPU frequency in MHz here if not defined in Makefile */
#define F_CPU 20000000UL
#include <util/delay.h>

/* 9600 baud */
#define UART_BAUD_RATE      9600      


int main(void)
{
    unsigned int c;
    char buffer[7];
    int  num=134;
	int  numm=1;
	
	//DDRD  |= (1 << PD1) | (1 << PD5) | (1 << PD6) | (1 << PD7);
	//PORTD |= (1 << PD5) | (1 << PD6) | (1 << PD7);

    
    /*
     *  Initialize UART library, pass baudrate and AVR cpu clock
     *  with the macro 
     *  UART_BAUD_SELECT() (normal speed mode )
     *  or 
     *  UART_BAUD_SELECT_DOUBLE_SPEED() ( double speed mode)
     */
    uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
    
    /*
     * now enable interrupt, since UART library is interrupt controlled
     */
    sei();
    
    /*
     *  Transmit string to UART
     *  The string is buffered by the uart library in a circular buffer
     *  and one character at a time is transmitted to the UART using interrupts.
     *  uart_puts() blocks if it can not write the whole string to the circular 
     *  buffer
     */
    //uart_puts("String stored in SRAM\n");
    
    /*
     * Transmit string from program memory to UART
     */
   // uart_puts_P("String stored in FLASH\n");
    
        
    /* 
     * Use standard avr-libc functions to convert numbers into string
     * before transmitting via UART
     */     
   // itoa( num, buffer, 10);   // convert interger into string (decimal format)         
   // uart_puts(buffer);        // and transmit string to UART

    
    /*
     * Transmit single character to UART
     */
	while(1){
    uart_putc('.');
	_delay_ms(1000);
	}
	    
    for(;;)
    {
        /*
         * Get received character from ringbuffer
         * uart_getc() returns in the lower byte the received character and 
         * in the higher byte (bitmask) the last receive error
         * UART_NO_DATA is returned when no data is available.
         *
         */
        c = uart_getc();
        if ( c & UART_NO_DATA )
        {
            /* 
             * no data available from UART 
             */
        }
        else
        {
            /*
             * new data available from UART
             * check for Frame or Overrun error
             */
            if ( c & UART_FRAME_ERROR )
            {
                /* Framing Error detected, i.e no stop bit detected */
                uart_puts_P("UART Frame Error: ");
            }
            if ( c & UART_OVERRUN_ERROR )
            {
                /* 
                 * Overrun, a character already present in the UART UDR register was 
                 * not read by the interrupt handler before the next character arrived,
                 * one or more received characters have been dropped
                 */
                uart_puts_P("UART Overrun Error: ");
            }
            if ( c & UART_BUFFER_OVERFLOW )
            {
                /* 
                 * We are not reading the receive buffer fast enough,
                 * one or more received character have been dropped 
                 */
                uart_puts_P("Buffer overflow error: ");
            }
            /* 
             * send received character twice back
             */
			//PORTD |= (0 << PD5);
            uart_putc( (unsigned char)c );
			uart_putc( (unsigned char)c );
        }
    }
    
}
