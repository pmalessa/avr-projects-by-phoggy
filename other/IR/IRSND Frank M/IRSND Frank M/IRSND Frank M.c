/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * irsndmain.c - demo main module to test irsnd encoder
 *
 * Copyright (c) 2010-2012 Frank Meyer - frank(at)fli4l.de
 *
 * ATMEGA88 @ 8 MHz
 *
 * Fuses: lfuse: 0xE2 hfuse: 0xDC efuse: 0xF9
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
#include <stdlib.h>
#include <avr/io.h>
#include "irsnd.h"
#include "uart.h"

#define UART_BAUD_RATE      9600    

#ifndef F_CPU
#  error F_CPU unkown
#endif

void
timer1_init (void)
{
#if defined (__AVR_ATtiny45__) || defined (__AVR_ATtiny85__)                // ATtiny45 / ATtiny85:
    OCR1C   =  (F_CPU / F_INTERRUPTS / 4) - 1;                              // compare value: 1/15000 of CPU frequency, presc = 4
    TCCR1   = (1 << CTC1) | (1 << CS11) | (1 << CS10);                      // switch CTC Mode on, set prescaler to 4
#else                                                                       // ATmegaXX:
    OCR1A   =  (F_CPU / F_INTERRUPTS) - 1;                                  // compare value: 1/15000 of CPU frequency
    TCCR1B  = (1 << WGM12) | (1 << CS10);                                   // switch CTC Mode on, set prescaler to 1
#endif

#ifdef TIMSK1
    TIMSK1  = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
#else
    TIMSK   = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
#endif
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * timer 1 compare handler, called every 1/10000 sec
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
ISR(TIMER1_COMPA_vect)
{
    (void) irsnd_ISR();                                                     // call irsnd ISR
    // call other timer interrupt routines here...
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * MAIN: main routine
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
int
main (void)
{
    IRMP_DATA irmp_data;
	unsigned int c;

	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
    irsnd_init();                                                             // initialize irsnd
    timer1_init();                                                            // initialize timer
    sei ();                                                                   // enable interrupts

	irmp_data.protocol = IRMP_NEC_PROTOCOL;
	irmp_data.flags    = 0;
		
    for (;;)
    {
		c = 0x0000;
        irmp_data.command  = 0x0000;
		c = uart_getc();
		if (c & UART_NO_DATA)
		{
		}		
		else
		{
			switch (c)
			{
			case 0x0041: irmp_data.address  = 0x5583; irmp_data.command = 0x0090;break;
			case 0x0042: irmp_data.address  = 0xFF00; irmp_data.command = 0x009C;break;
			case 0x0043: irmp_data.address  = 0x5583; irmp_data.command = 0x008C;break;
			case 0x0044: irmp_data.address  = 0xFF00; irmp_data.command = 0x0083;break;
			case 0x0045: irmp_data.address  = 0xFF00; irmp_data.command = 0x0082;break;
			case 0x0046: irmp_data.address  = 0xFF00; irmp_data.command = 0x009A;break;
			case 0x0047: irmp_data.address  = 0xFF00; irmp_data.command = 0x008E;break;
			}
			irsnd_send_data (&irmp_data, TRUE);
		}
	}
}