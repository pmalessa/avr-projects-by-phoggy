/*
 * SPI_Engineersgarage Master.c
 *
 * Created: 02.07.2012 14:48:33
 *  Author: Phil
 */ 

// Program to SPI (serial peripheral interface) using AVR microcontroller (ATmega16)
#include<avr/io.h>
#include<avr/interrupt.h>

#define F_CPU 16000000UL
#include<util/delay.h>
 
#define MOSI PB3
#define SS   PB2
#define SCK  PB5
uint8_t counter = 0;
 
void SPI_init()	//SPI initialization
{
DDRB |=(1<<MOSI) | (1<<SS) | (1<<SCK);	// set MOSI, SS and SCK as output pins
SPCR |=(1<<MSTR);	//enable MSTR
SPCR |=(1<<SPR0) | (1<<SPR1); //freq /128
SPCR |=(1<<SPE);    //Enable SPI
}

/* Timer1 initialisation */

void TIMER1_init(void)
{
	/* Clear Timer on Compare Match */
	TCCR1B |= (1 << WGM12);
	
	/* Compare OCIE1A Event is activated */
	TIMSK1 |= (1<<OCIE1A);
	
	/* Compare value 15625 */
	OCR1A = 1625 - 1;
	
	/* Prescaler 1024 */
	TCCR1B |= (1<<CS10) | (1<<CS12);
	
	/* Global interrupts enabled in main function */
	
	//TIMER1_COMPA_VECT will be activated 16000000/1024/15625 = every second
}
 
int main()
{
TIMER1_init();
SPI_init();	
sei();
while(1)
{
}
 
}
 


ISR(TIMER1_COMPA_vect)
{
SPDR = counter;
while(!(SPSR &(1<<SPIF)));	//wait until SPIF get high
if(counter < 254)counter++; else counter = 0;
}