/*
 * SPI Engineersgarage Slave.c
 *
 * Created: 01.07.2012 15:14:23
 *  Author: Phil
 */ 

// Program to SPI (serial peripheral interface) using AVR microcontroller (ATmega328p)
#include<avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 16000000UL
#include<util/delay.h>
 
#define MISO PB4
 
void SPI_init();
unsigned char received_from_spi(void);
 
int main()
{
 
DDRC=0xFF;
PORTC=0x00;
 
SPI_init();
sei();
while(1)
{
}
 
}
 
void SPI_init()	//SPI initialization
{
DDRB=(1<<MISO);	// set MISO as output pin, rest as input
SPCR=(1<<SPE) | (1<<SPIE);	// Enable SPI
}

unsigned char received_from_spi(void){
	return SPDR;
}

ISR(SPI_STC_vect)
{
  /*incoming[receivecounter++]*/ PORTC = received_from_spi();
  /*if (receivecounter >= BUFSIZE || incoming[receivecounter-1] == 0x00) {
    parse_message();
    receivecounter = 0;
  }*/
}