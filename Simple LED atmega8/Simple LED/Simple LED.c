/*
 * Simple_LED.c
 *
 * Created: 27.03.2012 19:21:44
 *  Author: Phil
 */

#include<avr/io.h>

#define F_CPU 8000000UL
#include<util/delay.h>

int main(void)
{
	//_delay_ms(1000); //Programmstartverzögerung
	
	DDRB |= (1 << PB0);
	int z= 60;
	
	while(1)
	{
		PORTB = (1 << PB0);
		long_delay(z);
		PORTB &= ~(1 << PB0);
		long_delay(z);
		}
	return 0;
}

void long_delay(uint16_t ms) {
    for (; ms>0; ms--) _delay_ms(1);
}